# License

## SPL (Sgalinski Plugin License)

Copyright (c) 2020 sgalinski Internet Services

Software released under this license is allowed to be used for commercial purposes. Permission is only granted to use the software. Any other actions, including copying, modifying, merging, publishing, distribution, sublicensing and / or selling copies are strictly prohibited!

The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.
