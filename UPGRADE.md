# Switch to version 6

Run `./sgc-core/install.sh` and let the install script create the eslint.config.mjs and prettier.config.mjs files.
If you previously changed something with those configurations you will have to manually migrate those changes.

For ESLint there's a guide: https://eslint.org/docs/latest/use/configure/migration-guide
For Prettier you just need to convert your JSON to the JavaScript object used in the new config.

# Switch to version 5

### Migrate from libsass to dart sass:

1. Do a dry run for in the migration script to check which files will be affected
   with ```sgc sassMigrator division "web/typo3conf/ext/project_theme/Resources/Public/Sass/*.scss" -nd```.
2. Run the division migration
   script ```sgc sassMigrator division "web/typo3conf/ext/project_theme/Resources/Public/Sass/*.scss" -d```
3. Run ```sgc css:compile``` and fix the remaining sass errors manually
4. Extend your .gitignore by the following two lines:

```
.eslintcache
.styleintcache
```

5. New eslint rules

Make sure to extend the eslint config from the sgc-core to enable new linting features.
You need to replace the whole file with the following content.

```javascript
import sgcCoreConfig from "./sgc-core/eslint.config.mjs";

export default [
	...sgcCoreConfig
];

```

6. New stylelint rules

Make sure to extend the stylelint config from the sgc-core to enable new linting features.
You need to replace the whole file with the following content.

```json
{
    "extends": "./sgc-core/stylelint-rules"
}
```

8. Execute "sgc build" at least three times as prettier is fixing multiple things not with the first call.

9. Add a prettier config file to your root directory for better IDE integration. ```prettier.config.mjs```

```javascript
export default require('./sgc-core/prettier.config.mjs');
```

Install the prettier extension for your IDE in order to automatically format your JS and CSS code.

- Jetbrains IDE's: https://plugins.jetbrains.com/plugin/10456-prettier
- VSCode: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

### New linting errors appearing

New Linting rules are now added for Sass and JavaScript files.
Syntax errors are automatically fixed when starting the compiling tasks without the ```--prod``` flag.
In example with your first regular build call.

### Setup PHPStorm for the linting tools
You can find the settings for eslint, stylelint and prettier in the [Trello - Tips & Tricks Board](https://trello.com/c/bZgaF1x0/136-phpstorm-scss-javascript-linting-stylelint-eslint-setup).

# Switch to version 4

Add a file .stylelintrc in your root folder with the following content. You can exchange the rules file to your needs.
If you have this file installed, exchange it's content.

```
{
	"extends": "./sgc-core/stylelint-rules"
}
```

If you have overwritten your .sgc-config.json, then ensure that you added the following to your library paths as
the latest entry:

```
"./sgc-core/node_modules"
```
