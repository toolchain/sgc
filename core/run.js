import hirestime from 'hirestime';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import logger from './logger.js';
import chalk from 'chalk';
import settings from './settings.js';
import process from 'node:process';

const argv = yargs(hideBin(process.argv)).argv;
const getElapsed = hirestime();

/**
 * Task Runner
 */
class Run {
	/**
	 * Kick things off
	 */
	constructor() {
		process.chdir('..');
		this._start();
	}

	/**
	 * Start the task runner
	 */
	async _start() {
		let taskArgument = Run.getTaskName();
		let taskNames = taskArgument.split(':');
		if (Object.prototype.hasOwnProperty.call(settings.tasks, taskNames[0])) {
			const taskModule = await settings.tasks[taskNames[0]]();
			const TaskClass = taskModule.default;
			await new TaskClass().run(taskNames[1] || null);
		} else {
			logger.error(`Task ${chalk.bold(taskNames[0])} is not defined`);
		}

		process.exit();
	}

	/**
	 * Returns the name of the triggered task
	 */
	static getTaskName() {
		return argv._[0] || 'default';
	}

	static processEnd() {
		logger.info(`SGC finished after ${getElapsed.s()}s`, true);
	}
}

// kickstart the runner
new Run();
// call _processEnd before exiting the program
process.on('exit', Run.processEnd.bind(null, { cleanup: true }));
