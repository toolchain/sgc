#!/bin/bash

if [ ! -e "./sgc-core/.sgc_version" ]; then
	echo -e "\033[0;31mThe .sgc-version file got lost. Please reinstall by executing the install.sh script.\033[0m"
	exit 1
fi

. ./sgc-core/core/version.conf
. ./sgc-core/.sgc_version
if [ "$SGC_VERSION" != "$INSTALLED_SGC_VERSION" ]; then
	echo -e "\033[0;31mYour installed sgc version does not match the one inside your project. Please execute the install.sh script inside the sgc-core folder. Run ./sgc-core/install.sh\033[0m"
	exit 1
fi

function setNodeVersion() {
	# the nvm config file will only exist on unix systems
	if [ ! $OSTYPE == "msys" ]; then
		. ~/.nvm/nvm.sh
	fi

	# include version config
	. ./sgc-core/core/node.sh

	if [ ! $OSTYPE == "msys" ]; then
		# set node version
		nvm use --delete-prefix "${nodeversion}"
	else
		# interestingly, setting the node version here will break on windows. for some reason the node and npm commands
		# wont't be available anymore afterwards. tell the user instead.
		if [ ! $(node -v) == "v${nodeversion}" ]; then
			echo -e "\033[0;31mWrong node version! Please run 'nvm use ${nodeversion}\033[0m"
			exit 1
		fi
	fi
}

COMMAND=$1

if [ "$COMMAND" == "shortlist" ]; then
	taskList="server images css css:svg css:abovethefold images:uploaded js:compile js shortlist releaseExtension css lint lint:js lint:css open sassMigrator sizeGuard sizeGuard:accept"
	if [ -e sgc-scripts ]; then
		echo -e "${taskList}" $(ls sgc-scripts | grep .sh$ | cut -f 1 -d .)
	else
		echo -e "${taskList}"
	fi
elif [ -e "sgc-core/core/modules/$COMMAND.sh" ]; then
	setNodeVersion
	"./sgc-core/core/modules/$COMMAND.sh" "$@"
elif [ -e "./sgc-scripts/$COMMAND.sh" ]; then
	setNodeVersion
	shift
	"./sgc-scripts/$COMMAND.sh" "$@"
elif [ -e "./sgc-scripts/$COMMAND.js" ]; then
	setNodeVersion
	node "./sgc-scripts/$COMMAND.js"
elif [ -e "./sgc-core/core/modules/$COMMAND.js" ]; then
	setNodeVersion
	node "./sgc-core/core/modules/$COMMAND.js" "$@"
else
	setNodeVersion
	cd sgc-core || exit
	node ./core/run.js "$@"
fi
