#!/bin/bash

echo -e "\n"
echo -e " \033[33m  ███████╗ ██████╗  ██████╗\033[0m"
echo -e " \033[33m  ██╔════╝██╔════╝ ██╔════╝\033[0m"
echo -e " \033[33m  ███████╗██║  ███╗██║     \033[0m"
echo -e " \033[33m  ╚════██║██║   ██║██║     \033[0m"
echo -e " \033[33m  ███████║╚██████╔╝╚██████╗\033[0m"
echo -e " \033[33m  ╚══════╝ ╚═════╝  ╚═════╝\033[0m\n"

. ./sgc-core/core/version.conf

echo -e "  \033[103m\033[30m                           \033[0m"
echo -e "  \033[103m\033[30m       Welcome to the      \033[0m"
echo -e "  \033[103m\033[30m sgalinski CLI Task Runner \033[0m"
echo -e "  \033[103m\033[30m          \033[1mv${SGC_VERSION}           \033[0m"
echo -e "  \033[103m\033[30m                           \033[0m"
echo -e "\n"

echo -e "\033[0;32mWelcome to the sgalinski CLI.\033[0m"

if [ ! -e ./sgc-core ]; then
	echo -e "\n\e[41m\e[97m Error: \e[49m"
	echo -e "\033[0;31m Wrong directory! Call this script from the directory that contains the sgc-core folder!\033[0m"
	echo -e "\033[0;31m This is usually your project root directory.\033[0m\n"
	exit 1
fi

echo -e "\033[0;32mSetup is in progress...\033[0m\n"

cd ./sgc-core || exit

if [ $OSTYPE == "msys" ]; then
	# nvm installation on windows cannot be automated
	if [ -z $(command -v nvm) ] >/dev/null; then
		echo -e "\n\e[41m\e[97m Error: \e[49m"
		echo -e "\033[0;31m You need to install the Node Version Manager for Windows before you can proceed with the installation.\033[0m"
		echo -e "\033[0;31m  ==> https://github.com/coreybutler/nvm-windows\033[0m\n"
		exit 1
	fi
else
	# check for nvm and install it if not present
	echo -e "\033[0;32mSetup Node Version Manager...\033[0m\n"
	mkdir -p ~/.nvm
	./core/installers/nvm.sh >/dev/null
	echo -e "\033[0;32mNVM installed ✓\033[0m\n"
	# include nvm
	unset NVM_IOJS_ORG_MIRROR NVM_NODEJS_ORG_MIRROR
	. ~/.nvm/nvm.sh
	# check if nvm should be updated
	if ! nvm --version | grep -q '0.40.1'; then
		echo -e "\033[0;32mUpdate to NVM 0.40.1 ✓\033[0m\n"
		(
			cd "$NVM_DIR"
			git fetch --tags origin
			git checkout $(git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1))
		) && \. "$NVM_DIR/nvm.sh"
	fi
fi

# include version config
. ./core/node.sh
echo -e "\033[0;34mInstall Node ${nodeversion}...\033[0m\n"

# set node version
nvm install ${nodeversion}

# nvm-windows won't set the version by default after installing it
if [ $OSTYPE == "msys" ]; then
	nvm use --delete-prefix ${nodeversion}
fi

echo -e "\033[0;32mNode installed ✓\033[0m\n"

# install node deps
echo -e "\033[0;34mInstall dependencies\033[0m"
rm -rf ./node_modules

npm set progress=false

if [ "$1" == "--production" ]; then
	npm ci --production --prefer-offline --no-audit &>/dev/null
else
	npm ci --prefer-offline --no-audit &>/dev/null
fi

# hacky, yes. comment remove th /dev/null thing if you need to debug
npm set progress=true
echo -e "\033[0;32mDependencies installed ✓\033[0m\n"

if [ "$1" == "--local" ]; then
	# install cli locally
	echo -e "\033[0;34mInstall sgc\033[0m"
	cp ./core/sgc ../sgc
else
	if [ -z $(command -v sgc) ] >/dev/null; then
		# install cli globally
		echo -e "\033[0;34mInstall sgc\033[0m"

		if [ $OSTYPE == "msys" ]; then
			cp ./core/sgc /c/Windows/System32
		else
			echo -e "\033[0;34mYou need to grant permission in order to install the sgc command\033[0m"
			if [ ! -e /usr/local/bin ]; then
				sudo mkdir -p /usr/local/bin
			fi
			sudo cp ./core/sgc /usr/local/bin
			if [ -e /etc/bash_completion.d ]; then
				# linux autocompletion path
				sudo cp ./core/autocompletion.sh /etc/bash_completion.d/sgc
				autocompletePath="/etc/bash_completion.d/sgc"
			elif [ -e /usr/local/etc/bash_completion.d ]; then
				# osx autocompletion path; if bash completion is installed
				sudo cp ./core/autocompletion.sh /usr/local/etc/bash_completion.d/sgc
				autocompletePath="/usr/local/etc/bash_completion.d/sgc"
			fi
		fi
	else
		echo -e "\033[0;32msgc is already installed globally ✓\033[0m\n"
	fi
fi

# store current version number
. ./core/version.conf
echo "INSTALLED_SGC_VERSION=$SGC_VERSION" >.sgc_version

config_json="../.sgc-config.json"
config_js="../.sgc-config.js"

if [ ! -e "$config_json" ] && [ ! -e "$config_js" ]; then
	echo -e "\033[33mNo .sgc-config.json or .sgc-config.js file found.\033[0m"
	echo -e "\033[33mDo you want me to create an example config-file? [Y/N]\033[0m"
	read -n 1 -r
	echo # move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		cp ./core/installers/example-sgc-config.js "$config_js"
		echo -e "\033[0;32mCreated .sgc-config.js ✓\033[0m\n"
	fi
fi

if [ ! -e ../.stylelintrc ]; then
	echo -e "\033[33mNo .stylelintrc file found.\033[0m"
	echo -e "\033[33mDo you want to use the sgalinski default configuration? [Y/N]\033[0m"
	read -n 1 -r
	echo # move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		cp ./core/installers/example-stylelintrc ../.stylelintrc
	fi
	echo -e "\033[0;32mCreated .stylelintrc ✓\033[0m\n"
fi

if [ ! -e ../eslint.config.mjs ]; then
	echo -e "\033[33mNo eslint.config.mjs file found.\033[0m"
	echo -e "\033[33mDo you want to use the sgalinski default configuration? [Y/N]\033[0m"
	read -n 1 -r
	echo # move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		cp ./core/installers/example-eslint.config.mjs ../eslint.config.mjs
	fi
	echo -e "\033[0;32mCreated eslint.config.mjs ✓\033[0m\n"

	if [ -e ../.eslintrc.js ]; then
		echo -e "\033[33mDo you want to delete your legacy .eslintrc.js configuration file? [Y/N]\033[0m"
		read -n 1 -r
		echo # move to a new line
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			rm ../.eslintrc.js
		fi
		echo -e "\033[0;32mRemoved legacy .eslintrc.js ✓\033[0m\n"
	fi
fi

if [ -e ../.eslintrc.js ]; then
	echo -e "\033[0;31mYou may need to manually migrate your existing .eslintrc.js configuration to eslint.config.mjs and delete .eslintrc.js!\033[0m"
	echo # move to a new line
fi

if [ ! -e ../prettier.config.mjs ]; then
	echo -e "\033[33mNo prettier.config.mjs file found.\033[0m"
	echo -e "\033[33mDo you want to use the sgalinski default configuration? [Y/N]\033[0m"
	read -n 1 -r
	echo # move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		cp ./core/installers/example-prettier.config.mjs ../prettier.config.mjs
	fi
	echo -e "\033[0;32mCreated prettier.config.mjs ✓\033[0m\n"

	if [ -e ../.prettierrc.js ]; then
		echo -e "\033[33mDo you want to delete your legacy .prettierrc.js configuration file? [Y/N]\033[0m"
		read -n 1 -r
		echo # move to a new line
		if [[ $REPLY =~ ^[Yy]$ ]]; then
			rm ../.prettierrc.js
		fi
		echo -e "\033[0;32mRemoved legacy .prettierrc.js ✓\033[0m\n"
	fi
fi

if [ -e ../.prettierrc.js ]; then
	echo -e "\033[0;31mYou may need to manually migrate your existing .prettierrc.js configuration to prettier.config.mjs and delete .prettierrc.js!\033[0m"
	echo # move to a new line
fi

# go back to the root folder
cd ..
echo -e "\033[0;32msgc installed ✓\033[0m\n"
echo -e "\033[0;32mYou can now use the 'sgc' command inside your project root.\033[0m\n"
echo -e "\033[0;32mRestart your shell in order to activate the autocompletion feature.\033[0m\n"

if [[ $SHELL == *"zsh"* && "$1" != "--local" ]]; then
	echo -e "\033[0;34mIt seems like you are using zsh.\033[0m"
	echo -e "\033[0;34mYou might want to put these lines in your .zshrc-file to enable sgc autocompletion\033[0m"

	echo -e "\n\tautoload bashcompinit"
	echo -e "\tbashcompinit"
	echo -e "\t. $autocompletePath\n"
fi
