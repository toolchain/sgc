export default {
	plugins: ['stylelint-prettier', 'stylelint-high-performance-animation'],
	extends: ['stylelint-config-standard-scss', 'stylelint-prettier/recommended'],
	rules: {
		'plugin/no-low-performance-animation-properties': [
			true,
			{
				ignore: 'paint-properties',
			},
		],
		'max-nesting-depth': [4, { ignore: ['pseudo-classes'] }],
		'selector-no-qualifying-type': null,
		'selector-max-id': 1,
		'selector-class-pattern': null,
		'scss/dollar-variable-pattern': null,
		'selector-max-compound-selectors': 4,
		'scss/double-slash-comment-whitespace-inside': null,
		'scss/no-global-function-names': null,
		'selector-id-pattern': null,
		'no-descending-specificity': null,
		'custom-property-pattern': null,
		'scss/comment-no-empty': null,
		'scss/at-if-no-null': null,
	},
};
