import hirestime from 'hirestime';
import fs from 'node:fs';
import path from 'node:path';
import chalk from 'chalk';
import * as mkdirp from 'mkdirp';
import * as globby from 'globby';
import esbuild from 'esbuild';
import Task from '../task.js';
import browserSync from 'browser-sync';
import deduplicateLegalCommentsPlugin from '../modules/deduplicate-legal-comments/index.js';
import minifyHTMLLiteralsPlugin from '../modules/minify-html-literals/index.js';

/**
 * The JS task takes care of compiling JavaScript and running qa on it
 */
export default class Js extends Task {
	/**
	 * Start the build process
	 *
	 * @override
	 */
	async run(_subTask = null) {
		const steps = [];
		this.getElapsed = hirestime();
		if (_subTask) {
			// if _subTask is set, just execute that one task
			if (_subTask === 'lint') {
				steps.push(this._runLint());
			} else if (_subTask === 'compile') {
				steps.push(this._runCompile());
			} else {
				this._logTaskNotDefined(`js:${_subTask}`);
			}
		} else {
			// otherwise execute everything
			if (this._config.jsPipeline.qa) {
				steps.push(this._runLint());
			}
			steps.push(this._runCompile());
		}
		await Promise.all(steps);
		this._logger.info(`Task ${chalk.bold('js')} finished after ${this.getElapsed.s()}s`);
	}

	/**
	 * Runs the qa task
	 *
	 * @param {string} subTask
	 * @returns {Promise<*>}
	 */
	async runSizeGuard(subTask = '') {
		const sizeGuardModule = await this._settings.tasks.sizeGuard();
		const SizeGuardTask = sizeGuardModule.default;
		return new SizeGuardTask().run(subTask);
	}

	/**
	 * Kicks off the compile task
	 */
	async _runCompile() {
		const isOnlyWatchedExtension = !this._config.extensions.some((extensionName) =>
			this._path.includes(extensionName),
		);

		if (isOnlyWatchedExtension && this._config.js.watcherEntryPoint) {
			// The extension is only watched, so trigger compilation of the main watcher entry point instead of the extension
			this._path = path.dirname(
				this._config.js.watcherEntryPoint.replace(
					this._config.directories.javascriptSrc,
					'',
				),
			);
		}

		const files = await globby.globby([
			`${this._path}/${this._config.directories.javascriptSrc}/*.js`,
		]);
		const compilations = [];
		files.forEach((file) => {
			compilations.push(this._compile(file, this._getOutputPath(file)));
		});
		await Promise.all(compilations);
		if (this._config.jsPipeline.qa && !this._embedded && this._config.sizeGuard.enabled) {
			await this.runSizeGuard();
		}
	}

	/**
	 * Returns the name of the output file
	 *
	 * @param {String} _fileName The name of the input file
	 */
	_getOutputPath(_fileName) {
		const newSuffix = this._config.jsPipeline.renameToDotMin
			? '.bundled.min.js'
			: '.bundled.js';
		const fileName = path.basename(_fileName).replace('.js', newSuffix);
		const distributionDirectory = this._getFullPath(this._config.directories.javascriptDest);
		return `${distributionDirectory}/${fileName}`;
	}

	/**
	 * Compiles and bundles the JS
	 *
	 * @param {String} _input The path to the input file
	 * @param {String} _output The path to the output file
	 */
	async _compile(_input, _output) {
		if (!this._isProduction()) {
			this._logger.displayBrowserSyncNotification('Compiling JS – hang in there...');
		}

		const outputDirectory = path.dirname(_output);
		if (!fs.existsSync(outputDirectory)) {
			mkdirp.sync(outputDirectory);
			this._logger.info(`Created directory ${chalk.dim(outputDirectory)}`);
		}

		try {
			await esbuild.build({
				entryPoints: [_input],
				nodePaths: this._config.js.libraryPaths,
				bundle: true,
				format: 'iife',
				outfile: _output,
				sourcemap: this._config.jsPipeline.sourceMaps,
				minify: this._config.jsPipeline.uglify,
				define: { global: 'window' },
				target: ['es2020'],
				...(this._isProduction()
					? {
							plugins: [minifyHTMLLiteralsPlugin, deduplicateLegalCommentsPlugin],
						}
					: {}),
			});
		} catch (error) {
			this._handleError(error.stack);
			return;
		}

		if (!this._isProduction()) {
			browserSync.reload('*.js');
		}

		this._logger.success(`Written ${chalk.white(path.basename(_output))}`);
		if (this._config.jsPipeline.sourceMaps) {
			this._logger.success(`Written ${chalk.white(`${path.basename(_output)}.map`)}`);
		}
	}

	/**
	 * Kicks off the QA task
	 */
	async _runLint() {
		const lintTask = await this._settings.tasks.lint();
		const JsTask = lintTask.default;
		await new JsTask().run('js');
	}
}
