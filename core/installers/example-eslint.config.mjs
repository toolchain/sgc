import sgcCoreConfig from './sgc-core/eslint.config.mjs';

export default [
	...sgcCoreConfig,
	{
		files: ['**/*.js', '**/*.ts'],
		languageOptions: {
			globals: {
				SgCookieOptin: true,
				// Activate if jQuery is enabled (Further environments: https://eslint.org/docs/latest/user-guide/configuring/language-options)
				// jQuery: true,
				// $: true
			},
		},
	},
];
