import hirestime from 'hirestime';
import chalk from 'chalk';
import * as globby from 'globby';
import scss from 'postcss-scss';
import stylelint from 'stylelint';
import { ESLint } from 'eslint';
import path from 'node:path';
import process from 'node:process';
import prettier from 'prettier';
import { promises as fs } from 'node:fs';
import LintingFormatter from '../lintingFormatter.js';
import Task from '../task.js';

/**
 * The Lint task reports all warnings and errors of your js and scss files.
 */
export default class Lint extends Task {
	/**
	 * Start the linting process
	 *
	 * @override
	 */
	async run(_subTask = null) {
		this.getElapsed = hirestime();
		const steps = [];
		if (_subTask) {
			if (_subTask === 'js') {
				steps.push(this._runJsLint());
			}

			if (_subTask === 'css') {
				steps.push(this._runCssLint());
			}
		} else {
			steps.push(this._runJsLint(), this._runCssLint());
		}
		await Promise.all(steps);
		this._logger.info(`Task ${chalk.bold('lint')} finished after ${this.getElapsed.s()}s`);
	}

	/**
	 * Kicks off the linting task
	 */
	async _runJsLint() {
		const files = await globby.globby([
			`${this._path}/${this._config.directories.javascriptSrc}/**/*.js`,
			`${this._path}/${this._config.directories.javascriptSrc}/**/*.ts`,
			'!**/*.min.js',
			...this._config.js.excludeFromQa,
		]);

		try {
			if (this._config.fix) {
				await this.runPrettier(files);
			}
			await this._jsLint(files);
		} catch (error) {
			this._handleError(error.stack);
		}
	}

	/**
	 * Runs linting tasks against all JS source files
	 *
	 * @param {Array} _files The array containing all src files
	 */
	async _jsLint(_files) {
		const eslint = new ESLint({
			fix: this._config.fix,
			cache: true,
			cwd: process.cwd(),
			overrideConfigFile: path.join(process.cwd(), 'eslint.config.mjs'),
		});

		const results = await eslint.lintFiles(_files);

		if (this._config.fix) {
			await ESLint.outputFixes(results);
		}

		const formatter = await eslint.loadFormatter(
			path.join(process.cwd(), './sgc-core/core/eslintFormatter.js'),
		);
		const resultText = formatter.format(results);

		if (resultText !== '') {
			console.log(resultText);
		}
	}

	/**
	 * Kicks off the CSS Linting Task
	 */
	async _runCssLint() {
		const files = await globby.globby([
			`${this._path}/${this._config.directories.sass}/**/*.scss`,
			...this._config.css.excludeFromQa,
		]);

		if (this._config.fix) {
			await this.runPrettier(files);
		}

		let stylelintResult;
		try {
			stylelintResult = await stylelint.lint({
				files,
				cache: true,
				customSyntax: scss,
				fix: this._config.fix,
			});
		} catch (error) {
			console.error(error.stack);
			return;
		}

		const { errored, results } = stylelintResult;

		if (!errored) {
			return;
		}

		let numberOfErrors = 0;
		results.forEach((result) => {
			if (!result._postcssResult) {
				return;
			}

			const { source, _postcssResult } = result;
			numberOfErrors += _postcssResult.messages.length;

			const error = {
				filePath: source,
				messages: _postcssResult.messages.map((message) => ({
					message: message.text.replace(` (${message.rule})`, ''),
					line: message.line,
					column: message.column,
					ruleId: message.rule,
				})),
			};
			if (error.messages.length > 0) {
				console.log(`${LintingFormatter.format(error)}`);
			}
		});

		if (numberOfErrors > 0) {
			this._logger.info(
				`You have ${chalk.red(
					numberOfErrors,
				)} linting-errors in your CSS. Please fix them!`,
			);
		}
	}

	/**
	 * Kicks of the prettier task
	 */
	async runPrettier(files) {
		let configFile = await prettier.resolveConfigFile(process.cwd());

		if (!configFile) {
			// Fallback configuration from sgc-core
			configFile = await prettier.resolveConfigFile(path.join(process.cwd(), 'sgc-core'));
		}

		const config = await prettier.resolveConfig(configFile);
		const calls = [];
		files.forEach((file) => {
			calls.push(this.prettify(file, { ...config, filepath: file }));
		});

		await Promise.all(calls);
	}

	/**
	 * Run prettier
	 *
	 * @param file
	 * @param config
	 * @returns {Promise<void>}
	 */
	async prettify(file, config) {
		const content = await fs.readFile(file, 'utf8');

		// Check if file is already formatted
		if (await prettier.check(content, config)) {
			return;
		}

		this._logger.info(`Running prettier on file ${file}`);
		const formatted = await prettier.format(content, {
			...config,
			...(file.includes('.scss') ? { trailingComma: 'none' } : {}),
		});
		await fs.writeFile(file, formatted);
	}
}
