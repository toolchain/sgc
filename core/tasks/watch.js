import Task from '../task.js';
import { watch } from 'chokidar';
import chalk from 'chalk';
import path from 'node:path';
import settings from '../settings.js';
import * as globby from 'globby';

/**
 * The watch-task observes changes in source files and triggers compilation
 */
export default class Watch extends Task {
	constructor() {
		super();
		this._isProcessing = false;
		this._debounceTimeout = null;
		this._debounceDelay = 1;
	}

	/**
	 * Runner function
	 *
	 * @override
	 */
	async run() {
		settings.setWatcherActive(true);
		return new Promise(() => {
			this._watchCss();
			this._watchJs();
		});
	}

	/**
	 * Watch for CSS-related files
	 */
	async _watchCss() {
		// Determine which extensions to watch
		const extensions =
			(this._config.watchExtensions || []).length > 0
				? this._config.watchExtensions
				: this._config.extensions;

		// Create glob patterns for each extension
		let globs = [];
		extensions.forEach((_extension) => {
			globs.push(
				this._getGlobFor(_extension, `${this._config.directories.sass}/**/*.scss`),
				this._getGlobFor(_extension, `${this._config.directories.svg}/**/*.svg`),
			);
		});

		// Negative patterns to exclude certain files
		const ignoredPatterns = ['!**/*_svg.scss'];

		// Use globby to resolve the final list of files
		const filesToWatch = await globby.globby([...globs, ...ignoredPatterns]);

		// Set up chokidar watcher
		const watcher = watch(filesToWatch, {
			ignoreInitial: true, // Only watch changes after initial scan
		});

		watcher.on('error', (error) => {
			this._handleError(error.stack);
		});

		watcher.on('change', this._onCssChange.bind(this));
		watcher.on('add', this._onNewFile.bind(this));
	}

	/**
	 * Watch for JS-related files
	 */
	async _watchJs() {
		// Determine which extensions to watch
		const extensions =
			(this._config.watchExtensions || []).length > 0
				? this._config.watchExtensions
				: this._config.extensions;

		let globs = [];
		extensions.forEach((_extension) => {
			globs.push(
				this._getGlobFor(_extension, `${this._config.directories.javascriptSrc}/**/*.js`),
				this._getGlobFor(_extension, `${this._config.directories.javascriptSrc}/**/*.ts`),
			);
		});

		// Negative patterns or exclude patterns
		const ignoredPatterns = ['!**/*.min.js', ...this._config.js.excludeFromQa];

		// Use globby to resolve the final list of files
		const filesToWatch = await globby.globby([...globs, ...ignoredPatterns]);

		// Set up chokidar watcher
		const watcher = watch(filesToWatch, {
			ignoreInitial: true,
		});

		watcher.on('error', (error) => {
			this._logger.error(error.stack);
		});

		// Map events similarly for JS
		watcher.on('change', this._onJsChange.bind(this));
		watcher.on('add', this._onNewFile.bind(this));
	}

	/**
	 * Generates the glob for files in _path in _extension
	 *
	 * @param {String} _extension Name of the extension
	 * @param {String} _path The path to the files inside the extension
	 */
	_getGlobFor(_extension, _path) {
		return path.join(this._config.directories.basePath, _extension, _path);
	}

	/**
	 * Debounced trigger function with lock mechanism
	 * @param {Function} triggerFunction The function to trigger (either _triggerCss or _triggerJs)
	 * @returns {Promise<void>}
	 */
	async _debouncedTrigger(triggerFunction) {
		if (this._isProcessing) {
			return;
		}

		clearTimeout(this._debounceTimeout);
		this._debounceTimeout = setTimeout(async () => {
			try {
				this._isProcessing = true;
				await triggerFunction.call(this);
			} finally {
				this._isProcessing = false;
			}
		}, this._debounceDelay);
	}

	/**
	 * Trigger CSS-compilation
	 */
	async _triggerCss() {
		const cssTaskModule = await this._settings.tasks.css();
		const CssTask = cssTaskModule.default;
		await new CssTask().run();
	}

	/**
	 * Trigger JS-compilation
	 */
	async _triggerJs() {
		const jsTaskModule = await this._settings.tasks.js();
		const JsTask = jsTaskModule.default;
		await new JsTask().run();
	}

	/**
	 * This function will execute on CSS file changes
	 *
	 * @param {String} _fileName The path to the file that changed
	 */
	_onCssChange(_fileName) {
		this._setCurrentExtensionName(_fileName);
		this._logger.info(`Change in ${chalk.white(path.basename(_fileName))}`);
		this._debouncedTrigger(this._triggerCss);
	}

	/**
	 * This function will execute on JS file changes
	 *
	 * @param {String} _fileName The path to the file that changed
	 */
	_onJsChange(_fileName) {
		this._logger.info(`Change in ${chalk.white(path.basename(_fileName))}`);
		this._debouncedTrigger(this._triggerJs);
	}

	/**
	 * Prints a notification about the creation of new files
	 *
	 * @param _fileName
	 * @private
	 */
	_onNewFile(_fileName) {
		this._logger.info(`New file ${chalk.white(path.basename(_fileName))}`);
	}

	/**
	 * Tells our settings object which extension shall be used for the following build steps
	 *
	 * @param {String} _file The changed file
	 */
	_setCurrentExtensionName(_file) {
		if (this._config.extensions[0] === '') {
			return;
		}
		// normalize windows backslashes
		let file = _file.replaceAll('\\', '/');
		settings.setExtension(file.split(this._config.directories.basePath)[1].split('/')[0]);
	}
}
