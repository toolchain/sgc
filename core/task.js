import process from 'node:process';
import { promisify } from 'node:util';
import fs from 'node:fs';
import path from 'node:path';
import * as mkdirp from 'mkdirp';
import chalk from 'chalk';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import settings from './settings.js';
import logger from './logger.js';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * This is the base class every task must extend
 */
export default class Task {
	/**
	 * Kick things off
	 */
	constructor(_options) {
		this._settings = settings;
		this._config = settings.config;
		this._embedded = _options?.embedded ?? false;
		this._path = this._settings.getPath();
		this._logger = logger;
		this.extensionName = settings.Extension;
		if (argv.prod || argv.production) {
			this._config.cssPipeline = this._config.css.pipeline.prod;
			this._config.jsPipeline = this._config.js.pipeline.prod;
			this._config.fix = false;
			this._config.mode = 'prod';
			logger.info(
				`Starting task ${chalk.bold(this.constructor.name.toLowerCase())} in prod mode`,
			);
		} else {
			this._config.cssPipeline = this._config.css.pipeline.dev;
			this._config.jsPipeline = this._config.js.pipeline.dev;
			this._config.fix = true;
			this._config.mode = 'dev';
			logger.info(
				`Starting task ${chalk.bold(this.constructor.name.toLowerCase())} in dev mode`,
			);
		}
	}

	/**
	 * Runs the task. A subTask can be triggered by appending it with a colon
	 *
	 * E.g: sgc css    – triggers the main Css Task
	 *      sgc css:qa – triggers the subTask qa
	 */
	run() {
		logger.error('Task is not implemented');
	}

	/**
	 * Logs the error message that _task could not be found
	 *
	 * @param {String} _task The task that could not be found
	 */
	_logTaskNotDefined(_task) {
		logger.error(`Task ${chalk.bold(_task)} is not defined`);
	}

	/**
	 * Returns the full path (relative to the project dir) for _path
	 *
	 * @param {String} _path The path the full path shall be found for
	 */
	_getFullPath(_path) {
		return `${this._path}/${_path}`;
	}

	/**
	 * Displays an augmented error message with information about the env
	 *
	 * @param _errorMessage
	 */
	_handleError(_errorMessage) {
		logger.error(
			chalk.bgRed(
				chalk.whiteBright(
					` Task ${chalk.black(this.constructor.name)} for extension ${chalk.black(
						this.extensionName,
					)} failed: `,
				),
			),
		);
		logger.error(_errorMessage);
		if (!settings.watcherActive) {
			process.exit(1);
		}
	}

	/**
	 * Writes _content to _filePath.
	 * It will create all necessary folders in _filePath if they don't exist already
	 *
	 * @param {string} _filePath The path that shall be written to
	 * @param {string} _content The file contents
	 */
	async _writeFile(_filePath, _content) {
		const directory = path.dirname(_filePath);
		if (!fs.existsSync(directory)) {
			mkdirp.sync(directory);
			logger.info(`Created directory ${chalk.dim(directory)}`);
		}
		const writeFile = promisify(fs.writeFile);
		return writeFile(_filePath, _content);
	}

	/**
	 * Checks if task is in production mode
	 * @returns {boolean}
	 */
	_isProduction() {
		return this._config.mode === 'prod';
	}
}
