import fs from 'node:fs';

const deduplicateLegalCommentsPlugin = {
	name: 'deduplicate-legal-comments',
	setup(build) {
		build.onEnd(async () => {
			const filePath = build.initialOptions.outfile;

			// Read the output file
			let contents = await fs.promises.readFile(filePath, 'utf8');

			// Match and collect unique license content, ignoring filenames
			const legalComments = new Set();
			const regex = /\/\*!\s+Bundled license information:([\s\S]*?)\*\//g;

			contents = contents.replaceAll(regex, (match, group) => {
				// Extract license blocks, removing filenames from each block
				const licenses = group.match(/\(\*![\s\S]*?\*\)/g) || [];
				licenses.forEach((license) => {
					// Remove filename references from the license block
					const cleanedLicense = license.replaceAll(
						/^.*?Bootstrap .*?\.js v/gm,
						'Bootstrap v',
					);
					legalComments.add(cleanedLicense.trim());
				});
				return ''; // Remove the matched bundled license block
			});

			// Build deduplicated legal comment header
			const deduplicatedHeader = [...legalComments].join('\n\n');
			const header = `/*${deduplicatedHeader}*/`;

			// Replace or prepend the deduplicated comments
			contents = `${header}\n` + contents;

			// Write back the modified file
			await fs.promises.writeFile(filePath, contents);
		});
	},
};

export default deduplicateLegalCommentsPlugin;
