import fs from 'node:fs';
import path from 'node:path';
import yargs from 'yargs';
import merge from 'merge';
import { hideBin } from 'yargs/helpers';
import { fileURLToPath } from 'node:url';
import process from 'node:process';

const argv = yargs(hideBin(process.argv)).argv;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let extension = '';
let watcherActive = false;

/**
 * This module is a global settings singleton
 * Every module can require this to access all global configurations
 */
class Settings {
	/**
	 * Kick things off
	 */
	constructor() {
		this._config = {};
		this._extensionPath = '';
		this._webPath = '';
		this._tasks = {
			css: () => import('./tasks/css.js'),
			js: () => import('./tasks/js.js'),
			watch: () => import('./tasks/watch.js'),
			server: () => import('./tasks/server.js'),
			images: () => import('./tasks/images.js'),
			open: () => import('./tasks/open.js'),
			default: () => import('./tasks/default.js'),
			build: () => import('./tasks/build.js'),
			lint: () => import('./tasks/lint.js'),
			sassMigrator: () => import('./tasks/sassMigrator.js'),
			sizeGuard: () => import('./tasks/sizeGuard.js'),
		};
	}

	async initialize() {
		this._config = await Settings.parseConfig();
		this._extensionPath = this._config.directories?.basePath || '';
		this._webPath = this._config.directories?.webPath || '';
		extension = this._config.extensions?.[0] || '';
		return this;
	}

	/**
	 * Returns the currently used extension path
	 *
	 * @param {Boolean} _isWebPath indicates if the requested path is for web, or file system access
	 */
	getPath(_isWebPath = false) {
		const extensionName = argv.ext === undefined ? extension : argv.ext;
		const basePath = _isWebPath ? this._webPath : this._extensionPath;

		if (!basePath || !extensionName) {
			return '';
		}

		return path.join(basePath, extensionName);
	}

	/**
	 * Set the current path to a new extension
	 *
	 * @param {String} _extensionName The name of the extension which path should be used
	 */
	setExtension(_extensionName) {
		extension = _extensionName;
	}

	/**
	 * Returns the name of the current extension
	 *
	 * @return {*}
	 * @constructor
	 */
	get Extension() {
		return extension;
	}

	/**
	 * Sets the flag that indicates if the current execution involves the watch task
	 *
	 * @param _value
	 */
	setWatcherActive(_value) {
		watcherActive = _value;
	}

	/**
	 * Returns the sgc-config object
	 */
	static async parseConfig() {
		let scriptsConfig;
		let projectConfig;

		const scriptsConfigPathJSON = path.resolve(__dirname, '../../sgc-scripts/.sgc-config.json');
		const scriptsConfigPathJS = path.resolve(__dirname, '../../sgc-scripts/.sgc-config.js');
		const projectConfigPathJS = path.resolve(__dirname, '../../.sgc-config.js');
		const projectConfigPathJSON = path.resolve(__dirname, '../../.sgc-config.json');

		try {
			if (fs.existsSync(scriptsConfigPathJS)) {
				const importedConfig = await import(scriptsConfigPathJS);
				scriptsConfig = importedConfig.default;
			} else if (fs.existsSync(scriptsConfigPathJSON)) {
				scriptsConfig = JSON.parse(fs.readFileSync(scriptsConfigPathJSON, 'utf8'));
			}

			if (fs.existsSync(projectConfigPathJS)) {
				const importedConfig = await import(projectConfigPathJS);
				projectConfig = importedConfig.default;
			} else {
				projectConfig = JSON.parse(fs.readFileSync(projectConfigPathJSON, 'utf8'));
			}

			return scriptsConfig ? merge.recursive(scriptsConfig, projectConfig) : projectConfig;
		} catch (error) {
			console.error('Error parsing configuration:', error);
			return {};
		}
	}

	/**
	 * Returns the object containing all tasks
	 */
	get tasks() {
		return this._tasks;
	}

	/**
	 * Returns the configuration object
	 */
	get config() {
		return this._config;
	}

	/**
	 * Returns true if a watcher is currently active
	 *
	 * @return {boolean}
	 */
	get watcherActive() {
		return watcherActive;
	}
}

// Create instance asynchronously
const instance = await new Settings().initialize();
Object.freeze(instance);

export default instance;
