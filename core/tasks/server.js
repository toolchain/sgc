import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import Task from '../task.js';
import browserSync from 'browser-sync';
import process from 'node:process';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * The JS task takes care of compiling JavaScript and running qa on it
 */
export default class Server extends Task {
	/**
	 * Start the server
	 *
	 * @override
	 */
	async run() {
		browserSync({
			proxy: this._config.browsersync.disableNonce
				? this._config.browsersync.url
					? this.getUrl()
					: undefined
				: {
						target: this._config.browsersync.url ? this.getUrl() : undefined,
						ws: false,
						proxyRes: [
							(proxyResponse) => {
								if (
									proxyResponse.headers['content-security-policy'] !== undefined
								) {
									proxyResponse.headers['content-security-policy'] =
										proxyResponse.headers['content-security-policy'].replace(
											'script-src',
											"script-src 'nonce-browser-sync'",
										);
								}

								return proxyResponse;
							},
						],
					},
			server: this._config.browsersync.server || undefined,
			middleware: (request, _response, next) => {
				let conjunction;
				conjunction = /\?/.test(request.url) ? '&' : '?';
				request.url = request.url + conjunction + this._config.browsersync.urlparams;
				next();
			},
			open: argv.s === undefined ? 'local' : false,
			snippetOptions: {
				rule: {
					match: /<\/head>/i,
					fn: function (snippet, match) {
						return snippet.replace('id=', `nonce="browser-sync" id=`) + match;
					},
				},
			},
		});
	}

	/**
	 * Determines the URL to open
	 */
	getUrl() {
		const configuredUrl = this._config.browsersync.url;
		const desiredDomain = argv.d;

		if (typeof configuredUrl === 'string') {
			return configuredUrl;
		}
		if (typeof configuredUrl === 'object') {
			if (desiredDomain === undefined) {
				for (const key in configuredUrl) {
					if (Object.prototype.hasOwnProperty.call(configuredUrl, key)) {
						return configuredUrl[key];
					}
				}
			} else {
				if (Object.prototype.hasOwnProperty.call(configuredUrl, desiredDomain)) {
					return configuredUrl[desiredDomain];
				}

				this._handleError(
					`The key "${desiredDomain}" could not be resolved ` +
						`to any domain. Please check your SGC configuration.`,
				);
			}
		}
	}
}
