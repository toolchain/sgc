import process from 'node:process';
import Task from '../task.js';
import open from 'open';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import chalk from 'chalk';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * Opens a defined URL in the default browser
 */
export default class Open extends Task {
	/**
	 * Runner function
	 *
	 * @override
	 */
	async run() {
		await new Promise((resolve) => {
			if (argv._.length > 1) {
				if (Object.prototype.hasOwnProperty.call(this._config, 'open')) {
					let sites = argv._[1];
					if (Object.prototype.hasOwnProperty.call(this._config.open, sites)) {
						let index = 0;
						this._config.open[sites].forEach((site) => {
							setTimeout(() => {
								open(site);
								if (this._config.open[sites].length === index - 1) {
									resolve();
								}
								// eslint-disable-next-line
							}, 100 * index++);
						});
					} else {
						this._logger.error(
							`The desired entry ${chalk.bold(sites)} could not be found in your .sgc-config.json!`,
						);
					}
				} else {
					this._logger.error(
						'You have currently no sites configured (set the "open" property inside your .sgc-config.json)!',
					);
				}
			} else {
				this._logger.error('Please specify which site you want to open!');
			}
		});
	}
}
