import { minifyHTMLLiterals } from 'minify-html-literals';
import fs from 'node:fs';

const minifyHTMLLiteralsPlugin = {
	name: 'deduplicate-legal-comments',
	setup(build) {
		build.onEnd(async () => {
			const filePath = build.initialOptions.outfile;

			// Read the output file
			let contents = await fs.promises.readFile(filePath, 'utf8');

			contents =
				minifyHTMLLiterals(contents, {
					fileName: filePath,
					shouldMinify: () => true,
				})?.code || contents;

			// Write back the modified file
			await fs.promises.writeFile(filePath, contents);
		});
	},
};

export default minifyHTMLLiteralsPlugin;
