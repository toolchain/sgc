import Task from '../task.js';
import { spawn } from 'node:child_process';
import fs from 'node:fs';

/**
 * This task handles image optimization
 */
export default class Images extends Task {
	/**
	 * Runner function
	 *
	 * @override
	 */
	async run(_subTask) {
		await new Promise((resolve) => {
			if (this._isProduction()) {
				this._logger.error('SGC images can not be run in production mode.');
				return;
			}

			let path = 'web/typo3conf/ext/project_theme/Resources/Public/Images';
			if (fs.existsSync('public/typo3conf/ext/project_theme/Resources/Public/Images')) {
				path = 'public/typo3conf/ext/project_theme/Resources/Public/Images';
			} else if (fs.existsSync('vendor/sgalinski/project-theme/Resources/Public/Images')) {
				path = 'vendor/sgalinski/project-theme/Resources/Public/Images';
			} else if (fs.existsSync('local/project_theme/Resources/Public/Images')) {
				path = 'local/';
			}

			if (_subTask === 'uploaded') {
				path = '';
			}

			const optimizer = spawn('./sgc-scripts/optimizeImages.sh', [path]);

			optimizer.stdout.on('data', (data) => {
				console.log(`${data}`);
			});

			optimizer.stderr.on('data', (data) => {
				console.log(`${data}`);
			});

			optimizer.on('error', (error) => {
				console.log(`Failed to optimize images:\n ${error.message}`);
			});

			optimizer.on('close', () => {
				resolve();
			});
		});
	}
}
