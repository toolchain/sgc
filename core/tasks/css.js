import hirestime from 'hirestime';
import * as sass from 'sass-embedded';
import fs from 'node:fs';
import path from 'node:path';
import * as globby from 'globby';
import postcss from 'postcss';
import cssnano from 'cssnano';
import autoprefixer from 'autoprefixer';
import { pathToFileURL } from 'node:url';
import chalk from 'chalk';
import { InlineSvg } from '../modules/inline-svg/index.js';
import { ImageDimensions } from '../modules/image-dimensions/index.js';
import { inlineSource } from 'inline-source';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import Task from '../task.js';
import browserSync from 'browser-sync';
import process from 'node:process';
import { Buffer } from 'node:buffer';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * The CSS task takes care of compiling Sass and running the CSS QA tasks
 */
export default class Css extends Task {
	/**
	 * Start the build process
	 *
	 * @override
	 */
	async run(_subTask = null) {
		const steps = [];
		this.getElapsed = hirestime();
		if (_subTask) {
			switch (_subTask) {
				case 'lint': {
					steps.push(this._runLint());

					break;
				}
				case 'compile': {
					steps.push(this._runCompile());

					break;
				}
				case 'svg': {
					steps.push(this._svg());

					break;
				}
				case 'abovethefold': {
					steps.push(this._abovethefold());

					break;
				}
				default: {
					this._logTaskNotDefined(`css:${_subTask}`);
				}
			}
		} else {
			if (this._config.cssPipeline.qa) {
				steps.push(this._runLint());
			}
			steps.push(this._runCompile());
		}
		await Promise.all(steps);
		this._logger.info(`Task ${chalk.bold('css')} finished after ${this.getElapsed.s()}s`);
	}

	/**
	 * Kicks off the compile task
	 */
	async _runCompile() {
		const isOnlyWatchedExtension = !this._config.extensions.some((extensionName) =>
			this._path.includes(extensionName),
		);
		let dependentTasks = isOnlyWatchedExtension ? [] : [this._svg()];
		await Promise.all(dependentTasks);

		if (isOnlyWatchedExtension && this._config.css.watcherEntryPoint) {
			// The extension is only watched, so trigger compilation of the main watcher entry point instead of the extension
			this._path = path.dirname(
				this._config.css.watcherEntryPoint.replace(this._config.directories.sass, ''),
			);
		}

		const files = await globby.globby([
			`${this._path}/${this._config.directories.sass}/*.scss`,
			'!**/_*.scss',
		]);
		const compilations = [];
		files.forEach((file) => {
			compilations.push(this._compile(file, this._getOutputPath(file)));
		});
		await Promise.all(compilations);
		dependentTasks = [this._abovethefold()];
		await Promise.all(dependentTasks);
		if (this._config.jsPipeline.qa && !this._embedded && this._config.sizeGuard.enabled) {
			await this.runSizeGuard();
		}
	}

	/**
	 * Runs the qa task
	 *
	 * @param {string} subTask
	 * @returns {Promise<*>}
	 */
	async runSizeGuard(subTask = '') {
		const sizeGuardModule = await this._settings.tasks.sizeGuard();
		const SizeGuardTask = sizeGuardModule.default;
		return new SizeGuardTask().run(subTask);
	}

	/**
	 * Kicks off the QA task
	 */
	async _runLint() {
		const LintTaskModule = await this._settings.tasks.lint();
		const CssTask = LintTaskModule.default;
		await new CssTask().run('css');
	}

	/**
	 * Returns the name of the output file
	 *
	 * @param {String} _fileName The name of the input file
	 */
	_getOutputPath(_fileName) {
		const newSuffix = this._config.cssPipeline.renameToDotMin ? '.min.css' : '.css';
		const fileName = path.basename(_fileName).replace('.scss', newSuffix);

		const distributionDirectory = this._getFullPath(this._config.directories.css);

		return `${distributionDirectory}/${fileName}`;
	}

	/**
	 * Returns the path to the sourceMap associated with _fileName
	 *
	 * @param {String} _fileName The name of the CSS-file the sourceMap is associated with
	 */
	_getSourceMapsPath(_fileName) {
		const fileName = path.basename(_fileName).replace('.css', '.css.map');

		const sourceMapDirectory = this._getFullPath(this._config.directories.cssSourceMaps);

		return `${sourceMapDirectory}/${fileName}`;
	}

	/**
	 * Compiles Sass to Css
	 *
	 * @param {String} _input The path to the input file
	 * @param {String} _output The path to the output file
	 */
	async _compile(_input, _output) {
		if (!this._isProduction()) {
			this._logger.displayBrowserSyncNotification('Compiling Sass – hang in there...');
		}

		const sourceMapPath = this._getSourceMapsPath(_output);
		const outFileName = path.basename(_output);
		const mapFileName = path.basename(sourceMapPath);

		const sassSettings = {
			outFile: _output,
			style: this._isProduction() ? 'compressed' : 'expanded',
			sourceMap:
				outFileName === 'abovethefold.min.css' ? '' : this._config.cssPipeline.sourceMaps,
			sourceMapIncludeSources:
				outFileName === 'abovethefold.min.css' ? '' : this._config.cssPipeline.sourceMaps,
			// https://github.com/twbs/bootstrap/issues/40962
			// If set to true, Sass won’t print warnings that are caused by dependencies (like bootstrap):
			// https://sass-lang.com/documentation/js-api/interfaces/options/#quietDeps
			quietDeps: true,
			silenceDeprecations: [
				'legacy-js-api',
				'mixed-decls',
				'color-functions',
				'global-builtin',
				'import',
			],
			importers: [
				{
					findFileUrl(url) {
						if (url.startsWith('web/typo3conf/')) {
							return new URL(pathToFileURL(url));
						}
						if (url.startsWith('public/typo3conf/')) {
							return new URL(pathToFileURL(url));
						}
						if (url.startsWith('vendor/')) {
							return new URL(pathToFileURL(url));
						}
						return null;
					},
				},
			],
			verbose: argv.v || argv.verbose,
		};

		if (argv.q || argv.quiet) {
			sassSettings.logger = sass.Logger.silent;
		}

		let compiledSass = { css: '' };
		try {
			compiledSass = await sass.compileAsync(_input, sassSettings);
		} catch (_error) {
			this._handleError(
				`${_error.message} in ${_error.file} on line ${_error.line}:${_error.column}`,
			);
		}

		const cssString = this._imageDimensions(compiledSass.css.toString());

		const postCssPlugins = [autoprefixer, ...(this._isProduction() ? [cssnano()] : [])];

		let postProcessedCss = { css: '', map: '' };

		try {
			postProcessedCss = await postcss(postCssPlugins).process(cssString, {
				from: _input,
				to: _output,
				map: this._config.cssPipeline.sourceMaps
					? {
							annotation:
								outFileName === 'abovethefold.min.css'
									? ''
									: sourceMapPath
											.replace(/^web/, '')
											.replace('/^public/', '')
											.replace(this._path + '/Resources/Public/', '../'),
							inline: false,
							prev: compiledSass.sourceMap,
						}
					: false,
			});
		} catch (_error) {
			this._handleError(_error.stack);
		}

		await this._writeFile(_output, postProcessedCss.css);
		if (this._config.cssPipeline.sourceMaps && postProcessedCss.map) {
			await this._writeFile(sourceMapPath, Buffer.from(postProcessedCss.map.toString()));
		}

		if (!this._isProduction()) {
			browserSync.reload([outFileName, mapFileName]);
		}

		this._logger.success(`Written ${chalk.white(outFileName)}`);
		if (this._config.cssPipeline.sourceMaps) {
			this._logger.success(`Written ${chalk.white(mapFileName)}`);
		}
	}

	/**
	 * Creates the inline-svg partial
	 */
	async _svg() {
		const svgFolderPath = this._getFullPath(this._config.directories.svg);
		if (fs.existsSync(svgFolderPath)) {
			const svgPartial = await InlineSvg.create(path.join(svgFolderPath, '**'), {
				template: 'sgc-core/inline-svg-template.mustache',
				interceptor: (svgData) =>
					Object.assign(svgData, {
						variableName: svgData.name.toLowerCase(),
						prefix: this._config.css.svgIconPrefix || '',
					}),
			});
			const partialFilePath = path.join(
				this._getFullPath(this._config.directories.sass),
				'_svg.scss',
			);
			this._logger.success(`Written ${chalk.white(path.basename(partialFilePath))}`);
			await this._writeFile(partialFilePath, svgPartial);
		}
	}

	/**
	 * Runs the whole CSS through the image dimensions module
	 *
	 * @param {String} _source The CSS as a string
	 */
	_imageDimensions(_source) {
		return new ImageDimensions(_source, {
			imageBasePath: this._getFullPath(this._config.directories.images),
		}).process();
	}

	/**
	 * Generates the abovethefold CSS
	 */
	async _abovethefold() {
		if (this._config.cssPipeline.abovethefold === false) {
			return;
		}
		let configs;
		configs = Object.prototype.hasOwnProperty.call(this._config.abovethefold, 0)
			? this._config.abovethefold
			: [this._config.abovethefold];

		await Promise.all(
			configs.map(async (config) => {
				const filename = path.basename(config.template);
				try {
					const html = await inlineSource(config.template, {
						compress: false,
					});

					const distributionDirectory = config.dest;
					await this._writeFile(path.join(distributionDirectory, filename), html);
				} catch (_error) {
					if (_error.message.includes('ENOENT: no such file')) {
						this._logger.info(
							`Could not find abovethefold assets for ${this.extensionName}. Skip task.`,
						);
					} else {
						this._handleError(_error.stack);
					}
				}
				this._logger.success(`Written ${chalk.white(path.basename(filename))}`);
			}),
		);
	}
}
