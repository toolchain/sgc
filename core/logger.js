import process from 'node:process';
import logSymbols from 'log-symbols';
import timestamp from 'time-stamp';
import chalk from 'chalk';
import yargs from 'yargs';
import browserSync from 'browser-sync';
import { hideBin } from 'yargs/helpers';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * Logging-Module for the SGC
 */
export default {
	info(_message, _force = false) {
		if ((argv.q || argv.quiet) && !_force) return;
		console.log(this.getTimestamp(), logSymbols.info, chalk.blue(_message));
	},

	warning(_message, _force = false) {
		if ((argv.q || argv.quiet) && !_force) return;
		console.log(this.getTimestamp(), logSymbols.warning, chalk.yellow(_message));
	},

	error(_message, _force = false) {
		if ((argv.q || argv.quiet) && !_force) return;
		console.log(this.getTimestamp(), logSymbols.error, chalk.red(_message));
	},

	success(_message, _force = false) {
		if ((argv.q || argv.quiet) && !_force) return;
		console.log(this.getTimestamp(), logSymbols.success, chalk.green(_message));
	},

	getTimestamp() {
		return chalk.dim(chalk.white(`[${timestamp('HH:mm:ss')}]`));
	},

	displayBrowserSyncNotification(_message) {
		browserSync.notify(`<b>SGC:</b> ${_message}`, 5000);
	},
};
