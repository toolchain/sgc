import chalk from 'chalk';
import * as globby from 'globby';
import path from 'node:path';
import fs from 'node:fs';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { spawn } from 'node:child_process';
import Task from '../task.js';
import process from 'node:process';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * This task handles the quality assurance
 */
export default class SizeGuard extends Task {
	async run(_subTask) {
		if (!this._config.sizeGuard) {
			this._logger.warning(`SizeGuard config not found in your sgc-config.`);
			return;
		}

		this.currentBranch = await this.getCurrentBranch();

		const steps = [];
		if (_subTask) {
			if (_subTask === 'accept') {
				steps.push(this.acceptSize());
			}
			if (_subTask === 'clear') {
				steps.push(this.createConfig());
			}
			if (_subTask === 'check') {
				steps.push(this.createConfig(false));
			}
		} else {
			steps.push(this.runSizeGuard());
		}
		await Promise.all(steps);
	}

	async runSizeGuard() {
		await this.createConfig(false);
		const config = this.loadBranchConfig();

		for (const filename of Object.keys(config)) {
			const fileInfo = config[filename];
			if (!fs.existsSync(fileInfo.path)) continue;

			const stableSize = await this.getStableFileSize(fileInfo.path);
			const oldSize = fileInfo.filesize;
			const diff = stableSize - oldSize;
			let diffPercentage = Math.round((1 - stableSize / oldSize) * 10_000) / 100;

			// No changes
			if (diff === 0) {
				continue;
			}

			// Size of file decreased
			if (diff < 0) {
				this._logger.success(
					`${filename} decreased ${diffPercentage}% (${Math.abs(diff / 1000)} kB) in size.`,
				);
				await this.acceptSize(filename, true);
				continue;
			}

			// File got bigger
			const fileExtension = path.extname(filename).replace('.', '');
			const maxIncrease = (this._config.sizeGuard[fileExtension] || {}).maxIncrease || 0;

			if (diff <= maxIncrease) {
				this._logger.info(
					`${filename} increased ${Math.abs(diffPercentage)}% (${
						diff / 1000
					} kB) and is below the threshold of ${maxIncrease / 1000} kB.`,
				);
				await this.acceptSize(filename, true);
				continue;
			}
			// Above the threshold
			this._logger.error(
				`${filename} increased ${Math.abs(diffPercentage)}% (${
					diff / 1000
				} kB) and is above the threshold (${
					maxIncrease / 1000
				} kB).\nPlease reduce your file size or accept this change with "sgc sizeGuard:accept ${filename}".`,
				false,
			);
		}
	}

	/**
	 * Accept the new file size for a minified file
	 *
	 * @param {string} file
	 * @param {boolean} hideMessage
	 * @returns {Promise<void>}
	 */
	async acceptSize(file = '', hideMessage = false) {
		const filename = argv._[1] || file;
		const config = this.loadBranchConfig();
		if (filename === '') {
			await this.createConfig();
			return;
		}

		if (!Object.prototype.hasOwnProperty.call(config, filename)) {
			this._logger.error(
				`File ${filename} was not found. Try to run ${chalk.bold('"sgc sizeGuard:clear"')}`,
				true,
			);
			return;
		}

		const stats = fs.statSync(config[filename].path);
		const previousSize = config[filename].filesize;
		config[filename].filesize = stats.size;
		config[filename].timestamp = stats.mtimeMs;

		if (!hideMessage) {
			this._logger.success(
				`Accepted new file size of ${chalk.bold(
					`${stats.size / 1000} kB`,
				)} for ${filename}. (Previously: ${chalk.bold(`${previousSize / 1000} kB`)})`,
			);
		}

		await this.saveConfig(config);
	}

	/**
	 * Returns the SizeGuard config for the current Branch or null if it doesn't exist
	 *
	 * @returns {null|*}
	 */
	loadBranchConfig() {
		const config = this.loadConfig();

		if (!config) {
			return null;
		}

		if (Object.prototype.hasOwnProperty.call(config, this.currentBranch)) {
			return config[this.currentBranch];
		}

		return null;
	}

	/**
	 * Loads the configuration file
	 *
	 * @returns {Object|null}
	 */
	loadConfig() {
		if (!this.existsConfig()) {
			return null;
		}
		return JSON.parse(fs.readFileSync(this._config.sizeGuard.filepath));
	}

	/**
	 * Saves the given config
	 *
	 * @param {object} branchConfig
	 * @returns {Promise<void>}
	 */
	async saveConfig(branchConfig) {
		const config = this.loadConfig();
		config[this.currentBranch] = branchConfig;
		fs.writeFileSync(this._config.sizeGuard.filepath, JSON.stringify(config, null, 4));
	}

	/**
	 * Creates a configuration
	 *
	 * @param {boolean} override
	 * @returns {Promise<void>}
	 */
	async createConfig(override = true) {
		if (!override && this.existsConfig() && this.loadBranchConfig()) {
			return;
		}

		let config = this.loadConfig();
		if (override || !config) {
			config = {};
		}

		const cssSizes = await this.getCssSizes();
		const jsSizes = await this.getJsSizes();
		config[this.currentBranch] = { ...cssSizes, ...jsSizes };

		fs.writeFileSync(this._config.sizeGuard.filepath, JSON.stringify(config, null, 4));
		this._logger.info(`Created new SizeGuard log file.`);
	}

	/**
	 * Checks if a configuration files exists
	 *
	 * @returns {boolean}
	 */
	existsConfig() {
		return fs.existsSync(this._config.sizeGuard.filepath);
	}

	/**
	 * Get all minified Css file sizes
	 *
	 * @returns {Promise<{}>}
	 */
	async getCssSizes() {
		const distribution = this._getFullPath(this._config.directories.css);
		const cssPaths = await globby.globby([`${distribution}/*.css`]);
		const sizes = await SizeGuard.getFileSizes(cssPaths);

		if (Object.keys(sizes).length === 0) {
			this._logger.info('No css files found. Run "sgc css" to compile your sass files.');
		}

		return sizes;
	}

	/**
	 * Get all minified Javascript file sizes
	 *
	 * @returns {Promise<{}>}
	 */
	async getJsSizes() {
		const distribution = this._getFullPath(this._config.directories.javascriptDest);
		const jsPaths = await globby.globby([`${distribution}/*.js`]);
		const sizes = await SizeGuard.getFileSizes(jsPaths);

		if (Object.keys(sizes).length === 0) {
			this._logger.info('No minified js files found. Run "sgc js" to compile your js files.');
		}

		return sizes;
	}

	/**
	 * Retrieve the sizes of the give paths with the timestamp and name of the file
	 *
	 * @param {string[]} paths
	 * @returns {Promise<{}>}
	 */
	static async getFileSizes(paths) {
		if (paths.length === 0) {
			return {};
		}

		const sizes = {};

		for (const path_ of paths) {
			const stats = fs.statSync(path_);
			const filename = path.basename(path_);
			sizes[filename] = {
				filesize: stats.size,
				timestamp: stats.mtimeMs,
				path: path_,
			};
		}

		return sizes;
	}

	/**
	 * Returns the current branch via the command "git branch --show-current"
	 *
	 * @returns {Promise<string>}
	 */
	async getCurrentBranch() {
		let currentBranch = '';
		await new Promise((resolve) => {
			const cmd = spawn('git', ['branch', '--show-current']);
			cmd.stdout.on('data', (data) => {
				currentBranch = data;
			});
			cmd.on('close', (code) => {
				if (code !== 0) {
					this._logger.error(`Task ${chalk.bold('sizeGuard')} failed.`, false);
				}
				resolve();
			});
		});
		return `${currentBranch}`.trim();
	}

	/**
	 * Gets the stable file size as sometimes the file contents are
	 * cleared temporarily, resulting in wrongly reported sizes.
	 *
	 * @param {string} filePath
	 * @param {number} retries
	 * @param {number} delayMs
	 * @returns {Promise<number>}
	 */
	async getStableFileSize(filePath, retries = 3, delayMs = 100) {
		for (let index = 0; index < retries; index++) {
			const stats = fs.statSync(filePath);
			if (stats.size > 0 || index === retries - 1) {
				return stats.size;
			}
			await new Promise((resolve) => setTimeout(resolve, delayMs));
		}

		return new Promise((resolve) => {
			const stats = fs.statSync(filePath);

			resolve(stats.size);
		});
	}
}
