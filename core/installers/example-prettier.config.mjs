import sgcCoreConfig from './sgc-core/prettier.config.mjs';

export default {
	...sgcCoreConfig,
};
