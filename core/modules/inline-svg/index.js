import path from 'node:path';
import mustache from 'mustache';
import fs from 'node:fs';
import pkg from 'xml2js-parser';
const { parseStringSync } = pkg;
import * as globby from 'globby';
import _ from 'underscore';
import svgToMiniDataURI from 'mini-svg-data-uri';
import imagemin from 'imagemin';
import imageminSvgo from 'imagemin-svgo';
import { Buffer } from 'node:buffer';

export class InlineSvg {
	/**
	 * Kick things off
	 *
	 * @param {String} _svgFilePath The path to the folder containing the SVGs
	 * @param {Object} _options Configuration object
	 */
	constructor(_svgFilePath, _options) {
		let defaultOptions = {
			template: './template.mustache',
			context: {},
			interceptor: null,
		};
		this._files = {};
		this._svgs = [];
		this._options = _.extend(defaultOptions, _options);
		this._svgFilePath = _svgFilePath;
	}

	static async create(_svgFilePath, _options) {
		const instance = new InlineSvg(_svgFilePath, _options);
		return instance._process(_svgFilePath);
	}

	/**
	 * Process all SVG files
	 *
	 * @param {string} _svgFilePath The path to the folder containing the SVGs
	 */
	async _process(_svgFilePath) {
		try {
			let svgs = await this._readFiles(_svgFilePath);
			let templateContent = this._getTemplateContent();

			// Convert string to Buffer before passing to imagemin
			let optimizedTemplateBuffer = await imagemin.buffer(Buffer.from(templateContent), {
				plugins: [imageminSvgo()],
			});

			svgs.forEach((svg) => this._processSvg(svg, _svgFilePath));
			let template = mustache.render(
				Buffer.from(optimizedTemplateBuffer).toString('utf8'),
				_.extend({}, this._options.context, {
					svgs: this._svgs,
				}),
			);
			return template;
		} catch (error) {
			console.error('Template processing error:', error);
			throw error;
		}
	}

	/**
	 * Processes a single SVG icon
	 *
	 * _data is expected to carry two properties:
	 *        data.content: A string containing the SVG source
	 *        data.fileName: A string containing the file name
	 *
	 * @param {Object} _data Object containing info about the SVG
	 * @param {String} _svgFilePath Original File Path from the fetched SVG
	 */
	_processSvg(_data, _svgFilePath) {
		const xmlString = parseStringSync(_data.content.toString(), {
			attrNameProcessors: [(name) => name.toLowerCase()],
		});
		const svgDimensions = this._getSvgDimensions(xmlString);
		const _svgFilePathAdjusted = _svgFilePath.replace('**', '');
		const svgData = {
			name: _data.fileName
				.replace(_svgFilePathAdjusted, '')
				.replaceAll('/', '-')
				.replace('.svg', '')
				.replace('fontawesome-', ''),
			// add fill="white" to allow later color changes based on this value
			inline: svgToMiniDataURI(
				_data.content.includes('fill=')
					? _data.content
					: _data.content.replace('<svg', '<svg fill="white"'),
			),
			width: Number.parseInt(svgDimensions.width) + 'px',
			height: Number.parseInt(svgDimensions.height) + 'px',
			dimensions: svgDimensions,
		};

		this._svgs.push(
			_.isFunction(this._options.interceptor)
				? _.extend({}, svgData, this._options.interceptor(svgData) || svgData)
				: svgData,
		);
	}

	/**
	 * Extracts the dimensions from the SVG source
	 *
	 * @param {Object} xmlString The parsed SVG XML object
	 * @returns {Object}
	 */
	_getSvgDimensions(xmlString) {
		const hasWidthHeightAttribute = xmlString.svg.$['width'] && xmlString.svg.$['height'];
		let width;
		let height;
		if (hasWidthHeightAttribute) {
			width = Number.parseInt(xmlString.svg.$['width']);
			height = Number.parseInt(xmlString.svg.$['height']);
		} else {
			width = Number.parseInt(
				xmlString.svg.$['viewbox']
					.toString()
					.replace(/^\d+\s\d+\s(\d+\.?[\d])\s(\d+\.?[\d])/, '$1'),
			);
			height = Number.parseInt(
				xmlString.svg.$['viewbox']
					.toString()
					.replace(/^\d+\s\d+\s(\d+\.?[\d])\s(\d+\.?[\d])/, '$2'),
			);
		}
		return {
			width,
			height,
		};
	}

	/**
	 * Reads all the SVGs that are located in _path
	 *
	 * @param {String} _path The location to read from
	 * @returns {Promise}
	 */
	async _readFiles(_path) {
		const files = await globby.globby(path.join(_path, '*.svg'));
		const svgs = [];
		files.forEach((_file) => {
			const content = fs.readFileSync(_file, 'utf8');
			svgs.push({
				fileName: _file,
				content: content,
			});
		});
		return svgs;
	}

	/**
	 * Returns the mustache template as a string
	 *
	 * @returns {String}
	 */
	_getTemplateContent() {
		return fs.readFileSync(this._options.template, 'utf8');
	}
}
