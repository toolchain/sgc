import Task from '../task.js';
import settings from '../settings.js';

/**
 * The main build step – basically a combination of the most important tasks
 */
export default class Build extends Task {
	/**
	 * Run all important tasks
	 *
	 * @override
	 */
	async run() {
		const cssModule = await this._settings.tasks.css();
		const jsModule = await this._settings.tasks.js();
		const sizeGuardModule = await this._settings.tasks.sizeGuard();

		const Css = cssModule.default;
		const Js = jsModule.default;
		const SizeGuard = sizeGuardModule.default;

		await Promise.all(
			this._config.extensions.map(async (_extension) => {
				settings.setExtension(_extension);
				const tasks = [];
				const cssTask = new Css({ embedded: true });
				tasks.push(cssTask.run());
				const jsTask = new Js({ embedded: true });
				tasks.push(jsTask.run());
				await Promise.all(tasks);
			}),
		);
		await new SizeGuard().run();
	}
}
