import Task from '../task.js';

/**
 * The default task will be executed if you don't pass a task argument to the runner
 */
export default class Default extends Task {
	/**
	 * Runner function
	 *
	 * @override
	 */
	async run() {
		const serverModule = await this._settings.tasks.server();
		const ServerTask = serverModule.default;
		await new ServerTask().run();

		const watchModule = await this._settings.tasks.watch();
		const WatchTask = watchModule.default;
		await new WatchTask().run();
	}
}
