export default {
	directories: {
		basePath: 'vendor/sgalinski/',
		webPath: 'vendor/sgalinski/',
		css: 'Resources/Public/StyleSheets',
		sass: 'Resources/Public/Sass',
		cssSourceMaps: 'Resources/Public/SourceMaps',
		javascriptSrc: 'Resources/Public/JavaScript',
		javascriptDest: 'Resources/Public/JavaScript/Dist',
		javascriptSourceMaps: 'Resources/Public/SourceMaps',
		images: 'Resources/Public/Images',
		sprites: 'Resources/Public/Images/Sprites',
		svg: 'Resources/Public/Images/Svg',
	},
	abovethefold: [
		{
			template:
				'vendor/sgalinski/project-theme/Resources/Private/Templates/Abovethefold/Src/PageRenderTemplate.html',
			dest: 'vendor/sgalinski/project-theme/Resources/Private/Templates/Abovethefold/Dist/',
		},
	],
	js: {
		libraryPaths: ['./vendor/sgalinski/project-theme/node_modules'],
		excludeFromQa: ['!**/Extensions/**/*', '!**/node_modules/**/*'],
		pipeline: {
			dev: {
				uglify: false,
				renameToDotMin: true,
				polyfills: true,
				qa: true,
				sourceMaps: true,
			},
			prod: {
				uglify: true,
				renameToDotMin: true,
				polyfills: true,
				qa: false,
				sourceMaps: false,
			},
		},
	},
	css: {
		excludeFromQa: ['!**/_svg.scss'],
		pipeline: {
			dev: {
				renameToDotMin: true,
				qa: true,
				sourceMaps: true,
			},
			prod: {
				renameToDotMin: true,
				qa: false,
				sourceMaps: false,
			},
		},
		svgIconPrefix: 'icon-',
	},
	supportedBrowsers: ['> 2%', 'ie 11'],
	images: {
		optimize: ['web/fileadmin', 'web/uploads'],
	},
	extensions: ['project-theme'],
	logMessages: {
		'imagesSuccess:uploaded': {
			level: 'error',
			message:
				'Please make sure to clear the TYPO3 cache now, image hashes might have changed. Also make sure to run this command again after a few days.',
		},
	},
	browsersync: {
		url: 'https://www.website-base.dev',
		urlparams: 'no_cache=1&disableScriptmerger=1',
	},
	open: {
		all: ['https://www.website-base.dev', 'https://www.website-base.dev/typo3'],
	},
	sizeGuard: {
		filepath: '.sgc-sizeGuard.log',
		css: {
			maxIncrease: 200,
		},
		js: {
			maxIncrease: 500,
		},
	},
};
