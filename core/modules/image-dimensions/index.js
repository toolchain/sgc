import path from 'node:path';
import imageSize from 'image-size';

export class ImageDimensions {
	constructor(_sourceCode, _options = {}) {
		if (!_options.imageBasePath) {
			_options.imageBasePath = './';
		}
		this._options = _options;
		this._sourceCode = _sourceCode;
	}

	process() {
		this._sourceCode = this._sourceCode.replaceAll(
			/image-height\([',"]([^)]+)[',"]\)/g,
			this._getHeight.bind(this),
		);
		this._sourceCode = this._sourceCode.replaceAll(
			/image-width\([',"]([^)]+)[',"]\)/g,
			this._getWidth.bind(this),
		);
		return this._sourceCode;
	}

	_getHeight(_codePart, _imagePath) {
		return `${imageSize(path.join(this._options.imageBasePath, _imagePath)).height}px`;
	}

	_getWidth(_codePart, _imagePath) {
		return `${imageSize(path.join(this._options.imageBasePath, _imagePath)).width}px`;
	}
}
