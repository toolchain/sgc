# image-dimensions

This module takes a string of CSS code and looks for the occurences of `image-width()` and `image-height()` and replaces them with the dimensions of the referenced images.

## Example

```css
// styles.css
body {
	background-image: url('background.jpg');
	width: image-width('background.jpg');
	height: image-height('background.jpg');
}
```

```js
// build.js
const fs = require('fs');
let css = fs.readFileSync('./foo.css', 'utf-8');
let newCss = new ImageDimensions(css, {
	imageBasePath: './images',
}).process();
fs.writeFileSync('./foo.css', newCss);
```

```css
// styles.css
body {
	background-image: url('background.jpg');
	width: 213px;
	height: 160px;
}
```

## Options

You can pass an option object as the second parameter. Right now the only option you can set is the `imageBasePath`. The `imageBasePath` will be prepended to the url that gets extracted from the `imageWidth` and `imageHeight` functions.
