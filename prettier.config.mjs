export default {
	singleQuote: true,
	bracketSpacing: true,
	trailingComma: 'all',
	useTabs: true,
	tabWidth: 4,
	arrowParens: 'always',
	printWidth: 100,
	semi: true,
};
