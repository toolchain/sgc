import logger from './logger.js';
import chalk from 'chalk';
import path from 'node:path';
import process from 'node:process';
import logSymbols from 'log-symbols';

/**
 * This module formats Linting results
 */
const LintingFormatter = {
	format(_error) {
		let formattedMessage = '';
		formattedMessage += logger.getTimestamp();
		formattedMessage += ' ';
		formattedMessage += logSymbols.warning;
		formattedMessage += ' ';
		formattedMessage += chalk.bgYellow(
			chalk.black(` ${path.relative(process.cwd(), _error.filePath)} `),
		);
		formattedMessage += '\n';
		_error.messages.forEach((_message) => {
			let position = `L ${_message.line}:${_message.column}`;
			let positionLength = position.length;
			for (let index = 10; index > positionLength; index--) {
				position = ' ' + position;
			}
			formattedMessage +=
				`${chalk.white(position)} ${chalk.yellow('⚠')} ${_message.message}` +
				`${chalk.white(` (Violated rule ${chalk.italic(_message.ruleId)})`)}\n`;
		});
		return formattedMessage;
	},
};

export default LintingFormatter;
