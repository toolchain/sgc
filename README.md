[![sgalinski Internet Services](https://www.sgalinski.de/fileadmin/files/for-external-usage/logo.svg  "sgalinski")](https://www.sgalinski.de/sgc/)

# sgalinski CLI Task Runner

> **Please note:**
> The CLI is intended to be used with bash or zsh.

## Installation

Move the 'sgc-core' folder inside your project root and execute the install script. If you are using composer, you
can install the sgc by running

```bash
composer require sgalinski/sgc-core
```

If you don't want to install the sgc inside the vendor directory, you can define an own directory in the extra
section of your main project composer.json.

```
"installer-paths": {
  "./sgc-core/": [
    "sgalinski/sgc-core"
  ]
}
```

Then run the installer:

```bash
./sgc-core/install.sh
```

### Local Installation

If you don't want to install the sgc command globally (e.g. in a server environment), just pass the --local flag to the
install script.

```bash
./sgc-core/install.sh --local
```

### Using Babel Polyfills [js.pipeline.{prod/dev}.polyfils]

Babel can provide you with some important Polyfills for next generation JavaScript
at compile time. To make this work, you need to install the following package
inside your project's JavaScript bundle.

Go to the folder where you store your local NPM-Dependencies (stuff like jQuery and other libs) and execute the
following command:

```bash
npm install @babel/polyfill
```

## Activation

If you purchased a license for the SGC, you will be provided with a
key that you need to set in your .sgc-config.json file.
Simply add a new field "license" at the bottom of the file and save your key here.

### Windows

> **Please note:**
> While it is theoretically possible to run the sgc on Windows, this option is neither thoroughly tested,
> nor is it advertised in any way. Things might have improved with Bash on Windows, but right now we do not have
> the capacity to actively provide a stable version for Windows. Don't hesitate
> to [get in touch with us](https://www.sgalinski.de/contact/)
> if you think you might be able to help out!

If you want to install the SGC on a Windows machine, there are some additional preparations are required:

* All commands are to be executed in **git-bash** window, do not try to use the cmd! You'll probably already have the
  git-bash
  installed if you are using git for Windows.
* Install NVM for Windows: https://github.com/coreybutler/nvm-windows
* You have to open git-bash with administrator privileges to run the installation script

## SGC Releases

Please note that the following steps need to be executed before a new release:

* Raise version number (according to SEMVER) in the following files:
* composer.json
* package.json
* core/version.conf
* Add a git-tag with the new version number

## Available Commands

* __sgc__: Loads a BrowserSync instance and listens for file changes (triggers css and js compilation)
    * __sgc -s__: Loads the BrowserSync instance, but prevents it from automatically open in the browser
    * __sgc -q__: Loads the BrowserSync instance, but disables desktop notifications
    * __sgc -d domain.dev__: Starts the BrowserSync instance with the specified dev-domain (must be configured in
      .sgc-config.json)
* __sgc css__: Runs the CSS QA task (linting) and compiles all CSS files
* __sgc js__: Runs the JS QA task (linting) and compiles all JS/TS files
* __sgc lint__: Runs the Linter for JS and Sass files
    * __sgc lint:js__: Runs the Linter for JS files
    * __sgc lint:css__: Runs the Linter for Sass files
* __sgc sassMigrater__: Migrates Libsass files to Dart Sass
* __sgc releaseExtension --ext {extension name}__: Starts a questionaire wich helps to update an extension according to
  SEMVER (updates composer.json and ext_emconf.php)
* __sgc images__: runs the image optimizer on all images inside the folder you specified in the directories section of
  the .sgc-config.json
* __sgc images:uploaded__: runs the image optimizer on all images inside the folder you specified in the images.optimize
  section of the .sgc-config.json
* __sgc open {sites}__ Opens a set of URLs in the default browser, that you can define inside your .sgc-config.json
* __sgc sizeGuard__: Checks if the file sizes don't increase above the threshold defined in the .sgc-config.json
    * __sgc sizeGuard:accept__: Accepts all changes
    * __sgc sizeGuard:accept {filename}__: Accepts the increased file size
    * __sgc sizeGuard:clear__: Removes and creates a new sizeGuard file
    * __sgc sizeGuard:check__: Creates a new sizeGuard file if non exists

You can call every command with the ```--production``` flag. This will prevent the toolchain from generating SourceMaps
and will not apply fixes from the linting tools.

## Configuration

To configure the frontend build process to your needs, you will have to modify the .sgc-config.json that the installer
will put into your projects root-directory.

> **Heads up**
> Prior to SGC 1.2.0, the config file was named config.json and lived inside the sgc-core/gulp folder. If this file is
> still present, sgc will use it instead of the .sgc-config.json. It is highly advised to use .sgc-config.json instead!

### directories

Holds the paths to the css, sass, javascript, images, sourcemaps and inline-svgs, relative to the extensions root.

* webPath: *path to your extension folder as seen from the web*
* basePath: *path to your extension folder as seen from the filesystem*

### abovethefold

Configuration for the critical path css.

* template: *path to the src template*
* dest: *path to the destination*

### js

The SGC will support you with writing next generation JavaScript and executing it in Browsers today, by transpiling it
to ECMAScript 6 compliant code. Currently ECMAScript 2021 Syntax and TypeScript are supported.

* compiler: *es2021|typescript*
* libraryPaths: *additional locations that should be searched when resolving CommonJS require statements*
* excludeFromQa: *glob patterns with locations that hold JavaScript that does not need to be linted (vendor stuff)
* watcherEntryPoint: entry point for compilation when any extension set inside `watchExtensions` is changed. Example: `vendor/sgalinski/project-theme/Resources/Public/JavaScript/main.js`

### css
* watcherEntryPoint: entry point for compilation when any extension set inside `watchExtensions` is changed. Example: `vendor/sgalinski/project-theme/Resources/Public/Sass/main.scss`

### images

* optimize: *locations of user uploaded images that should be optimized*

### extensions

A list of extensions that should be included in the watch and build task. Please note, that the very first extension in this list
is expected to be your project_theme.

### watchExtensions
Extensions that trigger compilation of the main watcher entry point instead of the extension code itself while the watcher is running. Only active for the task when the `watcherEntryPoint` is set, see above. Doesn't affect the build process.

Example:

```
watchExtensions: [
    'project-theme',
    'project-base',
    'sg-account',
    'sg-teaser',
    'sg-forms',
    'sg-jobs',
    'sg-youtube',
    'sg-vimeo',
    'project-theme-megamenu',
    'sg-news',
    'sg-comments',
    'df-tabs'
]
```

### browsersync

BrowserSync is a neat tool that will help you during the development and testing process. When you run `sgc`, it will
spin
up a small webserver that proxies the URL specified in your .sgc-config.json. If you change a JavaScript or Sass file
inside an extension that is on your watch list (see the option above), BrowserSync will automatically reload the page
or inject your changes directly into the browser. You can also open several instances of the BrowserSync URL in
different
browser windows and BS will take care of synchronizing all input and scroll events between them.

If you get errors with CSP, you have to add the nonce `"nonce-browser-sync"` to your `script-src` section of the CSP headers.
The nonce-specific behavior of the sgc can be deactivated by adding the `disableNonce` option with `true` as value when your instance doesn't use nonces.

### SizeGuard

SizeGuard is a pure visual tool for notifying you for file size changes. The threshold for your css and js changes can
be changed via your sgc-config.json. The threshold has to be defined in bytes.

````json
{
    "sizeGuard": {
        "css": {
            "maxIncrease": 100
        },
        "js": {
            "maxIncrease": 500
        }
    }
}
````

To disable the notification in your console, add following configuration to your sgc-config.json.

```json
{
    "sizeGuard": {
        "enabled": false
    }
}
```

## Usage

You can run a specific task on a specific component, or start a watcher that will fire up BrowserSync and run tasks when
source files change. Extensions that will be watched are defined in your .sgc-config.json file for each project.

If you run a specific task, you need to specify the extension you want the task to run on with the --ext parameter.

```bash
sgc css --ext project_base
```

There are a few available tasks you need to remember:

### default/watch task

```bash
sgc
```

Starts a project wide watcher that will trigger the css/javascript task when you change any .scss, or .js-file and opens
the website in your browser, as a browsersync session (changed js and css will automatically get injected when
recompiled).

*Hint:* If you already have a browsersync instance open in your browser, you can pass the argument -s to restart the
session without opening a browser.

To have changes inside extensions triggering recompilation, add them to the `watchExtensions` array in your configuration file.

### css:svg task (will be called by the css task automatically)

```bash
sgc css:svg --ext [extension name]
```

Triggers the generation of Sass-mixins for all SVGs inside the given directory. Each mixin will enable the usage of the
associated SVG as a css background image.

```scss
@import 'svg'; // imports the file with the generated mixins (inside your Sass-root)

.icon-globe {
    @include inline-svg($icon-globe); // inserts the file icon-globe.svg as an inlined background-image
}
```

### css:abovethefold (will be called by the css task automatically)

```bash
svg css:abovethefold
```

This task will read the html file you specified as abovethefold.template, read every <link> statement in it and replace
it with the css styles the referenced file contains, as an inline style tag. To make use of this feature, you should
create a separate css file in your project (abovethefold.scss -> abovethefold.css), as well as an html-template file
that references this stylesheet (note that the path needs to be relative to the template file). You can then set this
template file as
your [PageRenderTemplate](https://docs.typo3.org/typo3cms/TyposcriptReference/Setup/Config/Index.html#pagerenderertemplatefile)
.
All styles inside your abovethefold.css file will now be inlined directly into the HTML of your website. Note that you
should think about what to put in this file only styles that should be available directly after the render process on
_every_ page should go there.

### css task

```bash
sgc css --ext [extension name]
```

Triggers css compilation inside the given directory. Note, that this task will also run the sprite task before it
starts.

Assumptions:

* all scss files are inside the sass directory, relative to the given path.
* all css files go into the stylesheets directory, relative to the given path.

### images task

```bash
sgc images --ext [extension name]
```

Optimizes all images for the given path.

Assumptions:

* all images are inside the image directory, relative to the given path.

### optimize images in fileadmin and uploads

```bash
sgc images:uploaded
```

This tasks optimizes all images (png, jpg, gif, svg) inside the folders you specified in the sgc-config.json file. You
might want to
run this task on a regular basis to compress user uploaded media.

# Extending the sgc with your own modules

You can easily extend the sgc functionality by writing your own modules. Simply create a sgc-scripts folder next to
sgc-core
and put your custom scripts in there. Right now only shell-scripts with are supported, other languages might follow in
the future.

Execute your custom scripts by calling sgc <scriptName>

```
+ sgc-core
+ sgc-scripts
    - updateInstance.sh
    - updateDeployData.sh
```

```bash
sgc updateInstance
```

# Troubleshooting

## Autocompletion is not working

In order for autocompletion to work, your shell needs to know about the SGC autocompletion file. Depending on your
operating system, you will find this under ```/etc/bash_completion.d/sgc``` (Linux),
or ```/usr/local/etc/bash_completion.d/sgc``` (macOS). Usually, your shell will automatically read this file. If
autocompletion is not working, it is very likely, that for some reason it does not. To fix it, you can simply include it
in your ```.bashrc``` file:

```
source /usr/local/etc/bash_completion.d/sgc
[or]
source /etc/bash_completion.d/sgc
```

## I added the code to my ```.bashrc``` file, but autocompletion does still not work

On some macOS installations, bash won't read the ```.bashrc``` file on startup. To fix this, you can source the file
inside ```~/.bash_profile```:

```
source ~/.bashrc
```

## How to configure your .stylelint rules?

Add a file .stylelintrc in your root folder with the following content. You can exchange the rules file to your needs.

```
{
	"extends": "./sgc-core/stylelint-rules"
}
```

## TYPO3 Fluid template files are always cached when using the watcher URL

If you encounter this issue using the watcher at e.g., https://localhost:3000/, simply log in to the TYPO3 backend
at https://localhost:3000/typo3/ (an error message `Invalid referrer for /main` can be ignored) in a separate tab.

This way the URL parameters set inside the sgc-config.json should actually work:

Example:

```
    "browsersync": {
        "url": "<your-dev-domain>",
        "urlparams": "no_cache=1&disableScriptmerger=1"
    },
```

will mean that you can run https://localhost:3000/ and see the result
of `<your-dev-domain>?no_cache=1&disableScriptmerger=1`.
