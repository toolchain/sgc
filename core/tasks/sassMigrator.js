import hirestime from 'hirestime';
import chalk from 'chalk';
import { spawn } from 'node:child_process';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import process from 'node:process';
import Task from '../task.js';

const argv = yargs(hideBin(process.argv)).argv;

/**
 * Migrate libsass to Dart Sass
 */
export default class SassMigrator extends Task {
	/**
	 * Start the linting process
	 *
	 * @override
	 */
	async run() {
		this.getElapsed = hirestime();

		if (!argv._[1] || !argv._[2] || argv.h || argv.help) {
			console.log();
			console.log('Usage: sgc sassMigrator <migrator> <file> [options]');
			console.log(
				`Migrator types: ${chalk.blue('division')} | ${chalk.blue('module')} | ${chalk.blue('namespace')}`,
			);
			console.log('Options:');
			console.log(`\t${chalk.blue('-n, --dry-run')}\t\tDry run`);
			console.log(`\t${chalk.blue('-v, --verbose')}\t\tVerbose output`);
			console.log(
				`\t${chalk.blue('-d, --migrate-deps')}\tMigrate all dependencies found with imports`,
			);
			console.log(
				'\nInfo: Modules that are not found will be ignored and the script will continue running.\n',
			);
			return;
		}

		const settings = ['./sgc-core/node_modules/sass-migrator/sass-migrator.js', argv._[1]];

		if (argv.dryRun || argv.n) {
			settings.push('--dry-run');
		}

		if (argv.verbose || argv.v) {
			settings.push('--verbose');
		}

		if (argv.migrateDeps || argv.d) {
			settings.push('--migrate-deps');
		}

		settings.push(argv._[2]);
		await this.migrate(settings);

		this._logger.info(
			`Task ${chalk.bold('sassMigrator')} finished after ${this.getElapsed.s()}s`,
		);
	}

	async migrate(settings) {
		await new Promise((resolve) => {
			let hadError = false;

			const migrate = spawn('node', settings);

			migrate.stdout.on('data', (data) => {
				console.log(`${data}`);
			});

			migrate.stderr.on('data', (data) => {
				if (!hadError) {
					this._logger.warning(
						'Not found dependencies will be ignored when running the migrations.',
					);
					this._logger.warning('The Migrations might still be successful');
					hadError = true;
				}

				console.log(`${data}`);
			});

			migrate.on('error', (error) => {
				console.log(`Failed to run sassMigrator:\n ${error.message}`);
			});

			migrate.on('close', (code) => {
				if (code !== 0) {
					this._logger.error(`Task ${chalk.bold('sassMigrator')} failed.`);
				}
				resolve();
			});
		});
	}
}
