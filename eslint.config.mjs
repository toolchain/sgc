import unicorn from "eslint-plugin-unicorn";
import typescriptEslint from "@typescript-eslint/eslint-plugin";
import globals from "globals";
import tsParser from "@typescript-eslint/parser";
import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all
});

export default [...compat.extends(
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:unicorn/recommended",
    "prettier",
), {
    plugins: {
        unicorn,
        "@typescript-eslint": typescriptEslint,
    },

    languageOptions: {
        globals: {
            ...globals.browser,
            ...globals.commonjs,
        },

        parser: tsParser,
    },

    rules: {
        "@typescript-eslint/no-explicit-any": "error",
        "@typescript-eslint/no-empty-function": "off",
        "@typescript-eslint/no-require-imports": "error",
        "@typescript-eslint/no-var-requires": "off",
        "import/no-unresolved": "off",

        "no-plusplus": ["error", {
            allowForLoopAfterthoughts: true,
        }],

        "no-param-reassign": ["error", {
            props: false,
        }],

        "no-underscore-dangle": "off",
        radix: "off",

        "no-use-before-define": ["error", {
            functions: false,
        }],

        "unicorn/filename-case": "off",
        "unicorn/no-array-for-each": "off",
        "unicorn/consistent-function-scoping": "off",
        "unicorn/no-null": "off",
        "unicorn/no-array-callback-reference": "off",
        "unicorn/no-abusive-eslint-disable": "off",
		"unicorn/no-anonymous-default-export": "off",
		"unicorn/prefer-global-this": "off",
    },
}];
